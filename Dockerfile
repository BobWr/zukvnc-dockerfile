FROM ubuntu:latest
MAINTAINER baojk <bjklwr@outlook.com>

ENV UBUNTU 18.04

# 使用root用户
USER root

RUN apt-get update -qqy && apt-get install -y ca-certificates

RUN  echo "deb https://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse\n" > /etc/apt/sources.list \
 && echo "deb https://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse\n" >> /etc/apt/sources.list \
 && echo "deb https://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse\n" >> /etc/apt/sources.list \
 && echo "deb https://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse\n" >> /etc/apt/sources.list \
 && echo "deb https://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse\n" >> /etc/apt/sources.list

# 更新源,安装软件
RUN apt-get update -qqy && apt-get install -qqy openjdk-8-jdk
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update -qqy && apt-get install -qqy sudo xvfb vlc
RUN apt-get update -qqy && apt-get install -qqy software-properties-common \
 && add-apt-repository ppa:jonathonf/ffmpeg-4 \
 && apt-get install -qqy ffmpeg
RUN apt-get update -qqy && apt-get install -qqy language-pack-zh-hans  fonts-arphic-*
RUN apt-get update -qqy && apt-get install -qqy dbus
RUN /etc/init.d/dbus start

RUN export LANG="zh_CN.UTF-8"
RUN export LANGUAGE="zh_CN:zh:en_US:en"

# 添加并切换到zukvnc用户，此用户sudo无需密码
RUN useradd zukvnc --shell /bin/bash --create-home -U \
	&& echo "zukvnc ALL=(ALL)NOPASSWD: ALL" >> /etc/sudoers
USER zukvnc
RUN sudo apt-get install -qqy pulseaudio xdotool

WORKDIR /home/zukvnc
RUN mkdir -p ffmpeg/video

COPY chromedriver /usr/bin/chromedriver
COPY google-chrome-stable_74.0.3729.169-1_amd64.deb /home/zukvnc/google-chrome-stable_74.0.3729.169-1_amd64.deb
RUN sudo dpkg -i google-chrome-stable_74.0.3729.169-1_amd64.deb || sudo apt-get install -qqy -f

COPY zukvnc/.asoundrc .
COPY zukvnc/yahei.ttf /usr/share/fonts/TTF
COPY zukvnc/entrypoint.sh /home/zukvnc/entrypoint.sh
RUN sudo chmod +x entrypoint.sh
COPY zukvnc/vncsh /home/zukvnc/vncsh
RUN sudo chmod -R 777 vncsh

COPY starter-1.0.0-SNAPSHOT-fat.jar /home/zukvnc/starter-1.0.0-SNAPSHOT-fat.jar
RUN sudo chmod 777 /usr/bin/chromedriver

ENTRYPOINT [ "sh", "-c", "./entrypoint.sh" ]